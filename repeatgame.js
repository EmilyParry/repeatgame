/* code sources:
 
 SnailBait is referenced from.  Edits made by Emily Parry throughout.
 
 
 
 */
var RepeatGame = function ()
{
    this.canvas = document.getElementById('game-canvas'),
            this.context = this.canvas.getContext('2d'),
            this.fpsElement = document.getElementById('fps'),
            //Constants
            this.MAX_NUMBER_OF_LIVES = 1;
            this.lives = this.MAX_NUMBER_OF_LIVES;
            this.LEFT = 1,
            this.RIGHT = 2,
            this.SHORT_DELAY = 50; // milliseconds
            this.TRANSPARENT = 0,
            this.OPAQUE = 1.0,
            this.score = 0;
    
            //States
            this.gameStarted = false;
            this.playing = false;
    //Sprite sheet
    this.spritesheet = new Image(),
            //TIME
            this.lastAnimationFrameTime = 0,
            this.lastFpsUpdateTime = 0,
            this.fps = 60,
            this.fpsElement = document.getElementById('fps'),
            this.toastElement = document.getElementById('toast'),
            
            this.canvasHeight = 500, // height of canvas (e)
            this.canvasWidth = 900, // width of canvas (e)
            this.scoreElement = document.getElementById('repeatgame-score');
            
            this.PLAYER_EXPLOSION_DURATION = 500,
            this.playerTrack = this.STARTING_PLAYER_TRACK, //(e) 
    //
            //Background Foreground Images
            this.backgroundstar = new Image(),
            this.FOREGROUND_VELOCITY = 42, // speed the background is travelling at.
            this.RUN_ANIMATION_RATE = 10, // speed the player runs at.
            this.FOREGROUND_HEIGHT = 81,
            this.FOREGROUND_WIDTH = 1045,
            this.STARTING_FOREGROUND_VELOCITY = 0, // velocity(speed of it)
            this.STARTING_FOREGROUND_OFFSET = 10,
            this.STARTING_SPRITE_OFFSET = 0,
            this.foregroundOffset = this.STARTING_FOREGROUND_OFFSET, // Translation offset
            this.fgVelocity = this.STARTING_FOREGROUND_VELOCITY,
            this.spriteOffset = this.STARTING_SPRITE_OFFSET,
            this.PLATFORM_CELLS_HEIGHT = 73,
            this.PLATFORM_CELLS_WIDTH = 129,
            // this.FOREGROUND_POSITION = this.canvasHeight - this.FOREGROUND_HEIGHT+15, //(e)
            this.TRACK_1_BASELINE = 477,
            this.TRACK_2_BASELINE = 223,
            this.TRACK_3_BASELINE = 123,
            // Player 
           // this.playerTrack = this.STARTING_PLAYER_TRACK,
            //******SOUND***********
            this.coinSound = {
                position: 7.1, // seconds
                duration: 588, // milliseconds
                volume: 0.5
            },
    this.electricityFlowingSound = {
        position: 1.03, // seconds
        duration: 1753, // milliseconds
        volume: 0.5
    },
    this.explosionSound = {
        position: 4.3, // seconds
        duration: 760, // milliseconds
        volume: 1.0
    },
    this.pianoSound = {
        position: 5.6, // seconds
        duration: 395, // milliseconds
        volume: 0.5
    },
    this.thudSound = {
        position: 3.1, // seconds
        duration: 809, // milliseconds
        volume: 1.0
    },
    // SPRITE SIZES***********        
    this.PLAYER_CELLS_WIDTH = 47,
            this.PLAYER_CELLS_HEIGHT = 61,
            this.forSpriteOffset = 182.7, // (e) 

            this.CHICKEN_PACE_VELOCITY = 180, // speed of chicken
            this.PACE_VELOCITY = 100,
            this.CAT_CELLS_HEIGHT = 42,
            this.CAT_CELLS_WIDTH = 36,
            this.EXPLOSION_CELLS_WIDTH = 35,
            this.EXPLOSION_CELLS_HEIGHT = 44,
            this.ORB_CELLS_HEIGHT = 45,
            this.ORB_CELLS_WIDTH = 38,
            this.SLIME_CELLS_HEIGHT = 40,
            this.SLIME_CELLS_WIDTH = 40,
            this.HOUSE_CELLS_HEIGHT = 118,
            this.HOUSE_CELLS_WIDTH = 112,
            this.STAR_CELLS_HEIGHT = 30,
            this.STAR_CELLS_WIDTH = 30,
            this.CHICKEN_CELLS_HEIGHT = 38,
            this.CHICKEN_CELLS_WIDTH = 43,
            this.MOUNTAIN_HEIGHT = 204,
            this.MOUNTAIN_WIDTH = 850,
            this.MSHADOW_HEIGHT = 188,
            this.MSHADOW_WIDTH = 946,
            this.PLATFORM_VELOCITY_MULTIPLIER = 4.35,
            this.platformVelocity,
            // MUSIC**************************************
            this.soundAndMusicElement = document.getElementById('repeatGame-sound-and-music');
    this.musicElement = document.getElementById('repeatGame-music');
    this.musicElement.volume = 0.1;

    // SPRITE CELLS*********************************** 
    this.platformCells =
            [
                {left: 985, top: 470, width: this.PLATFORM_CELLS_WIDTH, heigth: this.PLATFORM_CELLS_HEIGHT}
            ];
    this.starCells = [
        {left: 991, top: 179, width: this.STAR_CELLS_WIDTH,
            height: this.STAR_CELLS_HEIGHT},
        {left: 1019, top: 178,
            width: this.STAR_CELLS_WIDTH,
            height: this.STAR_CELLS_HEIGHT}
    ];

    this.houseCells = [
        {left: 1323, top: 443, width: this.HOUSE_CELLS_WIDTH,
            height: this.HOUSE_CELLS_HEIGHT}
    ];

    this.catCells =
            [
                {
                    left: 999, top: 127, width: this.CAT_CELLS_WIDTH,
                    height: this.CAT_CELLS_HEIGHT
                },
                {
                    left: 1039, top: 127, width: this.CAT_CELLS_WIDTH,
                    height: this.CAT_CELLS_HEIGHT
                },
                {
                    left: 1076, top: 127, width: this.CAT_CELLS_WIDTH,
                    height: this.CAT_CELLS_HEIGHT
                },
                {
                    left: 1119, top: 127, width: this.CAT_CELLS_WIDTH,
                    height: this.CAT_CELLS_HEIGHT
                }

            ];
    this.explosionCells = [
        {left: 1189, top: 170, width: this.EXPLOSION_CELLS_WIDTH,
            height: this.EXPLOSION_CELLS_HEIGHT},
        {left: 1163, top: 171, width: this.EXPLOSION_CELLS_WIDTH,
            height: this.EXPLOSION_CELLS_HEIGHT},
        {left: 1138, top: 170, width: this.EXPLOSION_CELLS_WIDTH,
            height: this.EXPLOSION_CELLS_HEIGHT},
        {left: 1106, top: 172, width: this.EXPLOSION_CELLS_WIDTH,
            height: this.EXPLOSION_CELLS_HEIGHT}
    ],
    this.slimeCells =
            [
                {
                    left: 940, top: 19, width: this.SLIME_CELLS_HEIGHT,
                    height: this.SLIME_CELLS_HEIGHT
                },
                {
                    left: 980, top: 22, width: this.SLIME_CELLS_HEIGHT,
                    height: this.SLIME_CELLS_HEIGHT
                },
                {
                    left: 1019, top: 22, width: this.SLIME_CELLS_HEIGHT,
                    height: this.SLIME_CELLS_HEIGHT
                },
                {
                    left: 1060, top: 24, width: this.SLIME_CELLS_HEIGHT,
                    height: this.SLIME_CELLS_HEIGHT
                }

            ],
            this.chickenCells =
            [
                {left: 942, top: 74, width: this.CHICKEN_CELLS_WIDTH,
                    height: this.CHICKEN_CELLS_HEIGHT},
                {left: 981, top: 76, width: this.CHICKEN_CELLS_WIDTH,
                    height: this.CHICKEN_CELLS_HEIGHT},
                {left: 1021, top: 75, width: this.CHICKEN_CELLS_WIDTH,
                    height: this.CHICKEN_CELLS_HEIGHT},
                {left: 1057, top: 75, width: this.CHICKEN_CELLS_WIDTH,
                    height: this.CHICKEN_CELLS_HEIGHT},
                {left: 1096, top: 75, width: this.CHICKEN_CELLS_WIDTH,
                    height: this.CHICKEN_CELLS_HEIGHT},
                {left: 1141, top: 76, width: this.CHICKEN_CELLS_WIDTH,
                    height: this.CHICKEN_CELLS_HEIGHT}

            ];

    this.orbCells =
            [
                {
                    left: 1279, top: 69, width: this.ORB_CELLS_WIDTH,
                    height: this.ORB_CELLS_HEIGHT
                },
                {
                    left: 1319, top: 69, width: this.ORB_CELLS_WIDTH,
                    height: this.ORB_CELLS_HEIGHT
                }

            ];


    this.playerCellsRight =
            [{
                    left: 992, top: 320, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT

                },
                {
                    left: 1054, top: 320, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT
                },
                {
                    left: 1115, top: 321, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT
                },
                {
                    left: 1175, top: 319, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT
                },
                {
                    left: 1235, top: 320, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT
                },
                {
                    left: 1296, top: 320, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT
                },
                {
                    left: 1353, top: 321, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT
                }

            ];

    this.playerCellsLeft =
            [
                {
                    left: 997, top: 255, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT

                },
                {
                    left: 1054, top: 251, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT
                },
                {
                    left: 1113, top: 249, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT
                },
                {
                    left: 1175, top: 251, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT
                },
                {
                    left: 1235, top: 250, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT
                },
                {
                    left: 1293, top: 250, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT
                },
                {
                    left: 1353, top: 253, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT
                }
            ];

    //******SPRITE DATA************* (where they appear in the game)*********
    this.chickenData =
            [
                {left: -5, top: this.TRACK_1_BASELINE - this.CHICKEN_CELLS_HEIGHT}



            ];

    this.starData = [
        {left: 200,
            top: this.TRACK_1_BASELINE - this.PLAYER_CELLS_HEIGHT - 10},
        {left: 500,
            top: this.TRACK_1_BASELINE -this.PLAYER_CELLS_HEIGHT - 100},
        {left: 1050 - this.STAR_CELLS_WIDTH,
            top: this.TRACK_1_BASELINE - this.PLAYER_CELLS_HEIGHT},
        {left: 1500 + this.CAT_CELLS_WIDTH + 20,
            top: this.TRACK_1_BASELINE - this.PLAYER_CELLS_HEIGHT},
        {left: 1800,
            top: this.TRACK_1_BASELINE - this.PLAYER_CELLS_HEIGHT - 80},
        {left: 2400 - 50,
            top: this.TRACK_1_BASELINE - this.PLAYER_CELLS_HEIGHT}


    ];


    this.slimeData =
            [
                {
                    left: 1050, top: this.TRACK_1_BASELINE - this.PLAYER_CELLS_HEIGHT
                },
                {
                    left: 1800, top: this.TRACK_1_BASELINE - this.PLAYER_CELLS_HEIGHT
                },
                {
                    left: 1800 - this.SLIME_CELLS_WIDTH, top: this.TRACK_1_BASELINE - this.PLAYER_CELLS_HEIGHT
                },
                {
                    left: 2100, top: this.TRACK_1_BASELINE - this.PLAYER_CELLS_HEIGHT
                }
            ];

    this.orbData = [
        {
            left: 800, top: this.TRACK_1_BASELINE - this.ORB_CELLS_HEIGHT-this.PLAYER_CELLS_HEIGHT +20
        },
        {
            left: 800, top: this.TRACK_1_BASELINE - this.ORB_CELLS_HEIGHT - this.PLAYER_CELLS_HEIGHT - 85
        },
        {
            left: 1500, top: this.TRACK_1_BASELINE - this.PLAYER_CELLS_HEIGHT - 50
        },
        {
            left: 1500, top: this.TRACK_1_BASELINE - this.PLAYER_CELLS_HEIGHT - this.ORB_CELLS_HEIGHT - 50
        },
        {
            left: 2500, top: this.TRACK_1_BASELINE - this.PLAYER_CELLS_HEIGHT

        },
        {
            left: 2800, top: this.TRACK_1_BASELINE - this.PLAYER_CELLS_HEIGHT

        },
        {
            left: 3500, top: this.TRACK_1_BASELINE - this.PLAYER_CELLS_HEIGHT

        }
    ];

    this.catData = [
        {
            left: 500, top: this.TRACK_1_BASELINE - this.PLAYER_CELLS_HEIGHT
        },
        {
            left: 1500, top: this.TRACK_1_BASELINE - this.PLAYER_CELLS_HEIGHT
        },
        {
            left: 2250, top: this.TRACK_1_BASELINE - this.PLAYER_CELLS_HEIGHT
        }, {
            left: 2400, top: this.TRACK_1_BASELINE - this.PLAYER_CELLS_HEIGHT
        }
    ];

    this.houseData = [
        {left: 2800, top: this.TRACK_1_BASELINE - this.HOUSE_CELLS_HEIGHT}
    ];
    this.houses = [];
    this.cats = [];
    this.orbs = [];
    this.slimes = [];
    this.platforms = [];
    this.chickens = [];
    this.stars = [];
    this.sprites = []; // for when i add more sprites , contains all of sprites from arrays.
    //The sprite sheet co-ordinates for sprites

    //*****PLATFORMS**********
    this.platformData = [
        {left: 10,
            width: this.PLATFORM_CELLS_WIDTH,
            height: this.PLATFORM_CELLS_HEIGHT,
            track: 2
        },
        {
            left: 50,
            width: this.PLATFORM_CELLS_WIDTH,
            height: this.PLATFORM_CELLS_HEIGHT,
            track: 3

        }


    ];


    this.platformArtist = new SpriteSheetArtist(this.spritesheet,
            this.platformCells);



// **** BEHAVIORS*******************************************
    this.runBehavior = {
        lastAdvanceTime: 0,
        execute: function (sprite,
                now,
                fps,
                context,
                lastAnimationFrameTime) {
            if (sprite.runAnimationRate === 0) {
                return;
            }

            if (this.lastAdvanceTime === 0) {  // skip first time
                this.lastAdvanceTime = now;
            } else if (now - this.lastAdvanceTime >
                    1000 / sprite.runAnimationRate) {
                sprite.artist.advance();
                this.lastAdvanceTime = now;
            }
        }
    },
    
    this.paceBehaviorChicken = {
        setDirection: function (sprite) {


            sprite.direction = repeatGame.RIGHT;


        },
        setPosition: function (sprite, now, lastAnimationFrameTime) {
            var pixelsToMove = sprite.velocityX *
                    (now - lastAnimationFrameTime) / 1000;

            if (sprite.direction === repeatGame.RIGHT) {
                sprite.left += pixelsToMove;
            } else {
                sprite.left -= pixelsToMove;
            }
        },
        execute: function (sprite, now, fps, context,
                lastAnimationFrameTime) {
            this.setDirection(sprite);
            this.setPosition(sprite, now, lastAnimationFrameTime);
        }
    },
    this.paceBehavior =
            {
                setDirection: function (sprite) {


                    sprite.direction = repeatGame.LEFT;






                },
                setPosition: function (sprite, now, lastAnimationFrameTime) {
                    var pixelsToMove = sprite.velocityX *
                            (now - lastAnimationFrameTime) / 1000;

                    if (sprite.direction === repeatGame.RIGHT) {
                        sprite.left += pixelsToMove;
                    } else {
                        sprite.left -= pixelsToMove;
                    }
                },
                execute: function (sprite, now, fps, context,
                        lastAnimationFrameTime) {
                    this.setDirection(sprite);
                    this.setPosition(sprite, now, lastAnimationFrameTime);
                }



            },
            this.playerExplodeBehavior = new CellSwitchBehavior(
            this.explosionCells,
            this.PLAYER_EXPLOSION_DURATION,
            function (sprite, now, fps) { // Trigger
                return sprite.exploding;
            },
            function (sprite, animator) { // Callback
                sprite.exploding = false;
            }
    ),
            this.jumpBehavior = {
                pause: function (sprite) {
                    if (sprite.ascendTimer.isRunning()) {
                        sprite.ascendTimer.pause();
                    } else if (sprite.descendTimer.isRunning()) {
                        sprite.descendTimer.pause();
                    }
                },
                isAscending: function (sprite) {
                    return sprite.ascendTimer.isRunning();
                },
                ascend: function (sprite) {
                    var elapsed = sprite.ascendTimer.getElapsedTime(),
                            deltaY = elapsed / (sprite.JUMP_DURATION / 2) * sprite.JUMP_HEIGHT;

                    sprite.top = sprite.verticalLaunchPosition - deltaY; // Moving up
                },
                isDoneAscending: function (sprite) {
                    return sprite.ascendTimer.getElapsedTime() > sprite.JUMP_DURATION / 2;
                },
                finishAscent: function (sprite) {
                    sprite.jumpApex = sprite.top;
                    sprite.ascendTimer.stop();
                    sprite.descendTimer.start();
                },
                isDescending: function (sprite) {
                    return sprite.descendTimer.isRunning();
                },
                descend: function (sprite, verticalVelocity, fps) {
                    var elapsed = sprite.descendTimer.getElapsedTime(),
                            deltaY = elapsed / (sprite.JUMP_DURATION / 2) * sprite.JUMP_HEIGHT;

                    sprite.top = sprite.jumpApex + deltaY; // Moving down
                },
                isDoneDescending: function (sprite) {
                    return sprite.descendTimer.getElapsedTime() > sprite.JUMP_DURATION / 2;
                },
                finishDescent: function (sprite) {
                    sprite.top = sprite.verticalLaunchPosition;
                    sprite.stopJumping();
                    sprite.runAnimationRate = repeatGame.RUN_ANIMATION_RATE;
                },
                execute: function (sprite, now, fps, context,
                        lastAnimationFrameTime) {
                    if (!sprite.jumping || sprite.track === 3) {
                        return;
                    }

                    if (this.isAscending(sprite)) {
                        if (!this.isDoneAscending(sprite))
                            this.ascend(sprite);
                        else
                            this.finishAscent(sprite);
                    } else if (this.isDescending(sprite)) {
                        if (!this.isDoneDescending(sprite))
                            this.descend(sprite);
                        else
                            this.finishDescent(sprite);
                    }
                }
            },
    this.collideBehavior = {
        adjustScore: function (sprite) {
         if (sprite.value) {
            repeatGame.score += sprite.value;
            repeatGame.updateScoreElement();
         }
     },
        isCandidateForCollision: function (sprite, otherSprite) {
            var s, o;
         s = sprite.calculateCollisionRectangle(),
         o = otherSprite.calculateCollisionRectangle();
         return o.left < s.right && sprite !== otherSprite &&
               sprite.visible && otherSprite.visible && !sprite.exploding && 
               !otherSprite.exploding;
      
        },
        didCollide: function (sprite, otherSprite, context) {
           var s, o;
         s = sprite.calculateCollisionRectangle(),
         o = otherSprite.calculateCollisionRectangle();

         //Check whether any of the player's four corners or centre
         //lie in the other sprites bounding box
         context.beginPath();
         context.rect(o.left, o.top, o.right-o.left, o.bottom-o.top);
         return context.isPointInPath(s.left, s.top) ||
                context.isPointInPath(s.right, s.top) ||
                context.isPointInPath(s.centreX, s.centreY) ||
                context.isPointInPath(s.left, s.bottom) ||
                context.isPointInPath(s.right, s.bottom);
        },
       

        processBadGuyCollision: function (sprite) {
            
            repeatGame.explode(sprite);
         if(sprite.type === 'player')
         {
            repeatGame.loselife();
         }
         
        },
        processWinCollision:function(sprite)
        {
            repeatGame.win();
            
            
        },
        processAssetCollision: function (sprite) {
            sprite.visible = false; // sprite is the asset

            if (sprite.type === 'star') {
               repeatGame.playSound(repeatGame.coinSound);
               this.adjustScore(sprite);
            }

        },
        processCollision: function (sprite, otherSprite) {
             if ('star' === otherSprite.type) {
                this.processAssetCollision(otherSprite);
            } if ('cat' === otherSprite.type ||
                    'orb' === otherSprite.type ||
                    'slime' === otherSprite.type) {
                this.processBadGuyCollision(sprite);
            }
            if('house' === otherSprite.type)
            {
                this.processWinCollision(sprite);
                
            }
           
        },
        execute: function (sprite, now, fps, context,
                lastAnimationFrameTime) {
             var otherSprite;
         for(var i=0; i < repeatGame.sprites.length; ++i)
         {
            otherSprite = repeatGame.sprites[i];
            if(this.isCandidateForCollision(sprite, otherSprite))
            {
               if(this.didCollide(sprite, otherSprite, context))
               {
                  this.processCollision(sprite, otherSprite);
               }
            }
         }
        }
    };



};

RepeatGame.prototype = {
    createSprites: function ()
    {
        this.createPlatformSprites();
        this.createChickenSprites();
        this.createCatSprites();
        this.createSlimeSprites();
        this.createHouseSprites();
        this.createOrbSprites();
        this.createStarSprites();
        this.createPlayerSprite();


        // All sprites are also stored in a single array

        this.addSpritesToSpriteArray();
        this.initializeSprites();
    },
    addSpritesToSpriteArray: function () {

        for (var i = 0; i < this.platforms.length; ++i)
        {
            this.sprites.push(this.platforms[i]);
        }
        for (var i = 0; i < this.cats.length; ++i)
        {
            this.sprites.push(this.cats[i]);
        }
        for (var i = 0; i < this.houses.length; ++i)
        {
            this.sprites.push(this.houses[i]);
        }

        for (var i = 0; i < this.stars.length; ++i)
        {
            this.sprites.push(this.stars[i]);
        }
        for (var i = 0; i < this.slimes.length; ++i)
        {
            this.sprites.push(this.slimes[i]);
        }
        for (var i = 0; i < this.orbs.length; ++i)
        {
            this.sprites.push(this.orbs[i]);
        }
        for (var i = 0; i < this.chickens.length; ++i)
        {
            this.sprites.push(this.chickens[i]);
        }
        this.sprites.push(this.player);
    },
    positionSprites: function (sprites, spriteData) {
        var sprite;

        for (var i = 0; i < sprites.length; ++i) {
            sprite = sprites[i];


            if (spriteData[i].platformIndex) {
                this.putSpriteOnPlatform(sprite,
                        this.platforms[spriteData[i].platformIndex]);
            } else {
                sprite.top = spriteData[i].top;
                sprite.left = spriteData[i].left;
            }

        }
    },
    initializeSprites: function ()
    {
        this.positionSprites(this.stars, this.starData);
        this.positionSprites(this.slimes, this.slimeData);
        this.positionSprites(this.orbs, this.orbData);
        this.positionSprites(this.cats, this.catData);
        this.positionSprites(this.chickens, this.chickenData);
        this.positionSprites(this.houses, this.houseData);
        this.equipPlayer();
    },
    createPlatformSprites: function () {
        var sprite, pd;  // Sprite, Platform data

        for (var i = 0; i < this.platformData.length; ++i) {
            pd = this.platformData[i];

            sprite = new Sprite('platform', this.platformArtist);

            sprite.left = pd.left;
            sprite.width = pd.width;
            sprite.height = pd.height;


            sprite.track = pd.track;


            sprite.top = this.calculatePlatformTop(pd.track);

            this.platforms.push(sprite);

        }
    },
    createPlayerSprite: function () {
        var PLAYER_LEFT = 47,
                PLAYER_HEIGHT = 61,
                STARTING_RUN_ANIMATION_RATE = 0,
                STARTING_PLAYER_TRACK = 1;


        this.player = new Sprite('player', new SpriteSheetArtist(this.spritesheet,
                this.playerCellsRight), [this.runBehavior, this.jumpBehavior, this.collideBehavior, this.playerExplodeBehavior]);

        this.player.runAnimationRate = STARTING_RUN_ANIMATION_RATE;
        this.player.track = STARTING_PLAYER_TRACK;
        this.player.left = PLAYER_LEFT;
        this.player.top = this.calculatePlatformTop(this.player.track) - PLAYER_HEIGHT + 5;
        this.player.collisionMargin =
                {
                    left: 15,
                    top: 10,
                    right: 10,
                    bottom: 10
                };

        this.sprites.push(this.player);
    },
    createStarSprites: function () {
        var
                star,
                starArtist = new SpriteSheetArtist(this.spritesheet,
                        this.starCells);

        for (var i = 0; i < this.starData.length; ++i) {
            star = new Sprite('star',
                    starArtist);

            star.width = this.STAR_CELLS_WIDTH;
            star.height = this.STAR_CELLS_HEIGHT;
            star.value = 200;
            star.collisionMargin = {
                left: 5, top: 5, right:3, bottom: 5
            };
            this.stars.push(star);
        }
    },
    createHouseSprites: function () {
        var
                house,
                houseArtist = new SpriteSheetArtist(this.spritesheet,
                        this.houseCells);

        for (var i = 0; i < this.houseData.length; ++i) {
            house = new Sprite('house',
                    houseArtist);

            house.width = this.HOUSE_CELLS_WIDTH;
            house.height = this.HOUSE_CELLS_HEIGHT;

            house.collisionMargin = {
                left: 6, top: 11, right: 4, bottom: 8
            };
            this.houses.push(house);
        }
    },
    createCatSprites: function () // THESE WILL TAKE " SHOTS TO KILL
    {
        var
                cat,
                catArtist = new SpriteSheetArtist(this.spritesheet,
                        this.catCells);

        for (var i = 0; i < this.catData.length; ++i) {
            cat = new Sprite('cat',
                    catArtist, [
                        new CycleBehavior(500, // 300ms per image
                                3000) // 1.5 seconds interlude 
                    ]);

            cat.width = this.CAT_CELLS_WIDTH;
            cat.height = this.CAT_CELLS_HEIGHT;
            cat.collisionMargin = {
                left: 10, top: 10, right:10, bottom: 10
            };

            this.cats.push(cat);
        }
    },
    createOrbSprites: function () // THIS will not die. Created in a upwards linemust go over or under.
    {
        var
                orb,
                orbArtist = new SpriteSheetArtist(this.spritesheet,
                        this.orbCells);

        for (var i = 0; i < this.orbData.length; ++i) {
            orb = new Sprite('orb',
                    orbArtist, [
                        new CycleBehavior(300, // 300ms per image
                                1500),
                        this.paceBehavior// 1.5 seconds interlude 
                    ]);

            orb.width = this.ORB_CELLS_WIDTH;
            orb.height = this.ORB_CELLS_HEIGHT;
            orb.velocityX = this.PACE_VELOCITY;
            orb.collisionMargin = {
              left: 10, top: 10, right: 5, bottom: 10,
            };

            this.orbs.push(orb);
        }
    },
    createChickenSprites: function () {
        var chicken,
                chickenArtist = new SpriteSheetArtist(this.spritesheet,
                        this.chickenCells);

        for (var i = 0; i < this.chickenData.length; ++i) {
            chicken = new Sprite('chicken',
                    chickenArtist,
                    [
                        new CycleBehavior(300, // 300ms per image
                                1500), this.paceBehaviorChicken // 1.5 seconds interlude 
                    ]);

            chicken.width = this.CHICKEN_CELLS_WIDTH;
            chicken.height = this.CHICKEN_CELLS_HEIGHT;
            chicken.velocityX = this.CHICKEN_PACE_VELOCITY;

            this.chickens.push(chicken);
        }
    },
    createSlimeSprites: function () {
        var slime,
                slimeArtist = new SpriteSheetArtist(this.spritesheet,
                        this.slimeCells);

        for (var i = 0; i < this.slimeData.length; ++i) {
            slime = new Sprite('slime',
                    slimeArtist,
                    [
                        new CycleBehavior(300, // 300ms per image
                                1500) // 1.5 seconds interlude 
                    ]);

            slime.width = this.CHICKEN_CELLS_WIDTH;
            slime.height = this.CHICKEN_CELLS_HEIGHT;
            slime.collisionMargin = {
                left: 11, top: 10, right: 10, bottom: 10
            };

// SLIMES WILL BOUNCE
            this.slimes.push(slime);
        }
    },
    equipPlayerForJumping: function () {
        var INITIAL_TRACK = 1,
                PLAYER_JUMP_HEIGHT = 100,
                PLAYER_JUMP_DURATION = 800;

        this.player.JUMP_HEIGHT = PLAYER_JUMP_HEIGHT;
        this.player.JUMP_DURATION = PLAYER_JUMP_DURATION;

        this.player.jumping = false;
        this.player.track = INITIAL_TRACK;

        this.player.ascendTimer = new Stopwatch();
        this.player.descendTimer = new Stopwatch();

        this.player.jump = function () {
            if (this.jumping) // 'this' is the player
                return;

            this.jumping = true;
            this.runAnimationRate = 0; // Freeze the player while jumping
            this.verticalLaunchPosition = this.top;
            this.ascendTimer.start();
        };

        this.player.stopJumping = function () {
            this.descendTimer.stop();
            this.jumping = false;
        };
    },
    equipPlayer: function () {
        this.equipPlayerForJumping();
    },
    isSpriteInView: function (sprite) {
        return sprite.left + sprite.width > sprite.hOffset &&
                sprite.left < sprite.hOffset + this.canvas.width;
    },
    updateSprites: function (now) {
        var sprite;

        for (var i = 0; i < this.sprites.length; ++i) {
            sprite = this.sprites[i];

            if (sprite.visible && this.isSpriteInView(sprite)) {
                sprite.update(now,
                        this.fps,
                        this.context,
                        this.lastAnimationFrameTime
                        );
            }
        }
    },
    drawSprites: function () {
        var sprite;

        for (var i = 0; i < this.sprites.length; ++i) {
            sprite = this.sprites[i];

            if (sprite.visible && this.isSpriteInView(sprite)) {
                this.context.translate(-sprite.hOffset, 0);
                sprite.draw(this.context);
                this.context.translate(sprite.hOffset, 0);
            }
        }
    },
    drawbackgroundstar: function ()
    {
        this.context.drawImage(this.backgroundstar, 0, 0);
    },
    drawForeground: function ()
    {
        var FOREGROUND_TOP_IN_SPRITESHEET = 683; // Y POSITION IN SPRITESHEET
        var FOREGROUND_ACROSS_IN_SPRITESHEET = 941; // X POSITION IN SPRITESHEET

        this.context.translate(-this.foregroundOffset, 0);
        // 2/3 offscreen
        this.context.drawImage(
                this.spritesheet, FOREGROUND_ACROSS_IN_SPRITESHEET, FOREGROUND_TOP_IN_SPRITESHEET,
                this.FOREGROUND_WIDTH, this.FOREGROUND_HEIGHT,
                0, 440,
                this.FOREGROUND_WIDTH, this.FOREGROUND_HEIGHT);

        // Initially offscreen:
        this.context.drawImage(
                this.spritesheet, FOREGROUND_ACROSS_IN_SPRITESHEET, FOREGROUND_TOP_IN_SPRITESHEET,
                this.FOREGROUND_WIDTH, this.FOREGROUND_HEIGHT,
                this.FOREGROUND_WIDTH - 10, 440,
                this.FOREGROUND_WIDTH, this.FOREGROUND_HEIGHT);

        // Translate back to the original location
        this.context.translate(this.foregroundOffset, 0);

    },
    drawMountain: function ()
    {
        var MOUNTAIN_TOP_IN_SPRITESHEET = 278; // Y POSITION IN SPRITESHEET
        var MOUNTAIN_ACROSS_IN_SPRITESHEET = 50; // X POSITION IN SPRITESHEET

        this.context.translate(-this.foregroundOffset, 0);
        // 2/3 offscreen
        this.context.drawImage(
                this.spritesheet, MOUNTAIN_ACROSS_IN_SPRITESHEET, MOUNTAIN_TOP_IN_SPRITESHEET,
                this.MOUNTAIN_WIDTH, this.MOUNTAIN_HEIGHT,
                0, 280,
                this.MOUNTAIN_WIDTH, this.MOUNTAIN_HEIGHT);

        // Initially offscreen:
        this.context.drawImage(
                this.spritesheet, MOUNTAIN_ACROSS_IN_SPRITESHEET, MOUNTAIN_TOP_IN_SPRITESHEET,
                this.MOUNTAIN_WIDTH, this.MOUNTAIN_HEIGHT,
                this.MOUNTAIN_WIDTH, 280,
                this.MOUNTAIN_WIDTH, this.MOUNTAIN_HEIGHT);

        // Translate back to the original location
        this.context.translate(this.foregroundOffset, 0);

    },
    drawPlatforms: function () {
        var index;

        this.context.translate(-this.platformOffset, 0);

        for (index = 0; index < this.platformData.length; ++index) {
            this.drawPlatform(this.platformData[index]);
        }

        this.context.translate(this.platformOffset, 0);
    },
    drawMShadow: function ()
    {
        var SHADOW_TOP_IN_SPRITESHEET = 69; // Y POSITION IN SPRITESHEET
        var SHADOW_ACROSS_IN_SPRITESHEET = 4; // X POSITION IN SPRITESHEET

        this.context.translate(-this.foregroundOffset, 0);
        // 2/3 offscreen
        this.context.drawImage(
                this.spritesheet, SHADOW_ACROSS_IN_SPRITESHEET, SHADOW_TOP_IN_SPRITESHEET,
                this.MSHADOW_WIDTH, this.MSHADOW_HEIGHT,
                5, 290,
                this.MSHADOW_WIDTH, this.MSHADOW_HEIGHT);

        // Initially offscreen:
        this.context.drawImage(
                this.spritesheet, SHADOW_ACROSS_IN_SPRITESHEET, SHADOW_TOP_IN_SPRITESHEET,
                this.MSHADOW_WIDTH, this.MSHADOW_HEIGHT,
                this.MSHADOW_WIDTH - 20, 290,
                this.MSHADOW_WIDTH, this.MSHADOW_HEIGHT);

        // Translate back to the original location
        this.context.translate(this.foregroundOffset, 0);

    },
    draw: function (now) {
        this.setPlatformVelocity();
        this.setOffsets(now);
        this.drawbackgroundstar();
        this.drawMShadow();
        this.drawMountain();


        this.drawForeground();
        this.updateSprites(now);
        this.drawSprites();



    },
    setPlatformVelocity: function ()
    {
        this.platformVelocity = this.fgVelocity * this.PLATFORM_VELOCITY_MULTIPLIER; /* so the platforms are always scrolling velocity multiplier * the forground.  */
    },
    setOffsets: function (now) {
        this.setForegroundOffset(now);
        this.setSpriteOffsets(now);

    },
    setSpriteOffsets: function (now)
    {
        var sprite;


        this.spriteOffset +=
                this.platformVelocity * (now - this.lastAnimationFrameTime) / 1000;

        for (var i = 0; i < this.sprites.length; ++i) {
            sprite = this.sprites[i];

            if ('player' !== sprite.type) {
                sprite.hOffset = this.spriteOffset;
            }
        }

    },
    setForegroundOffset: function (now)
    {
        this.foregroundOffset +=
                this.fgVelocity * (now - this.lastAnimationFrameTime) / 1000;

        if (this.foregroundOffset < 0 ||
                this.foregroundOffset > this.FOREGROUND_WIDTH) {
            this.foregroundOffset = 0;
        }

    },
    putSpriteOnPlatform: function (sprite, platformSprite) {
        sprite.top = platformSprite.top - sprite.height;
        sprite.left = platformSprite.left;
        sprite.platform = platformSprite;
    },
    calculatePlatformTop: function (track) {
        if (track === 1) {
            return this.TRACK_1_BASELINE;
        } else if (track === 2) {
            return this.TRACK_2_BASELINE;
        } else if (track === 3) {
            return this.TRACK_3_BASELINE;
        }
    },
    /*
     * Was used before i added Platforms
     * 
     * calculatePlayerTop: function (track) {
     if (track === 1) {
     return this.TRACK_1_BASELINE;
     } // 323 pixels
     else if (track === 2) {
     return this.TRACK_2_BASELINE;
     } // 223 pixels
     else if (track === 3) {
     return this.TRACK_3_BASELINE;
     } // 123 pixels
     }, */
    calculateFps: function (now) {
        var fps = 1 / (now - this.lastAnimationFrameTime) * 1000;

        if (now - this.lastFpsUpdateTime > 1000) {
            this.lastFpsUpdateTime = now;
            this.fpsElement.innerHTML = fps.toFixed(0) + ' fps';
        }
        return fps;
    },
    turnLeft: function () {
        this.fgVelocity = -this.FOREGROUND_VELOCITY;
        this.player.runAnimationRate = this.RUN_ANIMATION_RATE;
        this.player.artist.cells = this.playerCellsLeft;
    },
    turnRight: function () {
        this.fgVelocity = this.FOREGROUND_VELOCITY;
        this.player.runAnimationRate = this.RUN_ANIMATION_RATE;
        this.player.artist.cells = this.playerCellsRight;
    },
    
   fadeInElements: function () {
      var args = arguments;

      for (var i=0; i < args.length; ++i) {
         args[i].style.display = 'block';
      }

      setTimeout( function () {
         for (var i=0; i < args.length; ++i) {
            args[i].style.opacity = repeatGame.OPAQUE;
         }
      }, this.SHORT_DELAY);
   },

   fadeOutElements: function () {
      var args = arguments,
          fadeDuration = args[args.length-1]; // Last argument

          for (var i=0; i < args.length-1; ++i) {
            args[i].style.opacity = this.TRANSPARENT;
         }

         setTimeout(function() {
            for (var i=0; i < args.length-1; ++i) {
               args[i].style.display = 'none';
            }
         }, fadeDuration);
      },

   hideToast: function () {
      var TOAST_TRANSITION_DURATION = 450;

      this.fadeOutElements(this.toastElement, 
         TOAST_TRANSITION_DURATION); 
   },

   startToastTransition: function (text, duration) {
      this.toastElement.innerHTML = text;
      this.fadeInElements(this.toastElement);
   },

   revealToast: function (text, duration) {
      var DEFAULT_TOAST_DURATION = 1000;

      duration = duration || DEFAULT_TOAST_DURATION;

      this.startToastTransition(text, duration);

      setTimeout( function (e) {
         repeatGame.hideToast();
      }, duration);
   },
   revealInitialToast: function () {
      var INITIAL_TOAST_DELAY = 1500,
      INITIAL_TOAST_DURATION = 3000;

      setTimeout( function () {
         repeatGame.revealToast('       ******Chase THAT CHICKEN HOME!! Avoid enemies and collect the stars!******', 
           INITIAL_TOAST_DURATION);
      }, INITIAL_TOAST_DELAY);
   },
   
    animate: function (now) {

        repeatGame.fps = repeatGame.calculateFps(now);
        repeatGame.draw(now);
        repeatGame.lastAnimationFrameTime = now;
        requestNextAnimationFrame(repeatGame.animate);
    },
    backgroundLoaded: function () {
        var LOADING_SCREEN_TRANSITION_DURATION = 2000;

        setTimeout(function () {
            repeatGame.startGame();
            repeatGame.gameStarted = true;
        }, LOADING_SCREEN_TRANSITION_DURATION);
    },
    initializeImages: function ()
    {
        this.spritesheet.src = 'images/spritesheet.png';
        this.backgroundstar.src = 'images/star.png';

        this.spritesheet.onload = function (e) {
            repeatGame.backgroundLoaded();
        };

    },
    createAudioChannels: function () {
        var channel;

        for (var i = 0; i < this.audioChannels.length; ++i) {
            channel = this.audioChannels[i];

            if (i !== 0) {
                channel.audio = document.createElement('audio');
                channel.audio.addEventListener('loadeddata', // event
                        //'progress',
                        this.soundLoaded, // callback
                        false);           // use capture

                channel.audio.src = this.audioSprites.currentSrc;
            }

            channel.audio.autobuffer = true;
        }
    },
    seekAudio: function (sound, audio) {
        try {
            audio.pause();
            audio.currentTime = sound.position;
        } catch (e) {
            if (console) {
                console.error('Cannot seek audio');
            }
        }
    },
    playAudio: function (audio, channel) {
        try {
            audio.play();
            channel.playing = true;
        } catch (e) {
            if (console) {
                console.error('Cannot play audio');
            }
        }
    },
    soundLoaded: function () {

        repeatGame.audioSpriteCountdown--;

        if (repeatGame.audioSpriteCountdown === 0) {
            if (!repeatGame.gameStarted && repeatGame.graphicsReady) {
                repeatGame.startGame();
            }
        }
    },
    getFirstAvailableAudioChannel: function () {
        for (var i = 0; i < this.audioChannels.length; ++i) {
            if (!this.audioChannels[i].playing) {
                return this.audioChannels[i];
            }
        }

        return null;
    },
    playSound: function (sound) {
        var channel,
                audio;

        if (this.soundOn) {
            channel = this.getFirstAvailableAudioChannel();

            if (!channel) {
                if (console) {
                    console.warn('All audio channels are busy. ' +
                            'Cannot play sound');
                }
            } else {
                audio = channel.audio;
                audio.volume = sound.volume;

                this.seekAudio(sound, audio);
                this.playAudio(audio, channel);

                setTimeout(function () {
                    channel.playing = false;
                    repeatGame.seekAudio(sound, audio);
                }, sound.duration);
            }
        }
    },
    explode: function (sprite) {
         if ( ! sprite.exploding) {
         if (sprite.runAnimationRate === 0) {
            sprite.runAnimationRate = this.RUN_ANIMATION_RATE;
         }

         sprite.exploding = true;
         repeatGame.playSound(repeatGame.explosionSound);
      }
        
    },
   loseLife: function()
   {
      var TRANSITION_DURATION = 3000;

      this.lives--;
      this.startLifeTransition(repeatGame.PLAYER_EXPLOSION_DURATION);

      setTimeout( function () { // After the explosion
         repeatGame.endLifeTransition();
      }, TRANSITION_DURATION); 
   },

   startLifeTransition: function (slowMotionDelay) {
      var CANVAS_TRANSITION_OPACITY = 0.05,
          SLOW_MOTION_RATE = 0.1;

      this.canvas.style.opacity = CANVAS_TRANSITION_OPACITY;
      this.playing = false;

      setTimeout( function () {
         repeatGame.setTimeRate(SLOW_MOTION_RATE);
         repeatGame.player.visible = false;
      }, slowMotionDelay);
   },

   endLifeTransition: function () {
      var TIME_RESET_DELAY = 1000,
          RUN_DELAY = 500;

      repeatGame.reset();

      setTimeout( function () { // Reset the time
         repeatGame.setTimeRate(1.0);

         setTimeout( function () { // Stop running
            repeatGame.player.runAnimationRate = 0;
            repeatGame.playing = true;
         }, RUN_DELAY);
      }, TIME_RESET_DELAY);
   },

      resetPlayer: function () {
      this.player.left      = repeatGame.PLAYER_LEFT;
      this.player.track     = 3;
      this.player.hOffset   = 0;
      this.player.visible   = true;
      this.player.exploding = false;
      this.player.jumping   = false;
      this.player.top       = this.calculatePlatformTop(3) -
                            this.player.height;

      this.player.artist.cells     = this.playerCellsRight;
      this.player.artist.cellIndex = 0;
   },

   resetOffsets: function () {
      this.fgVelocity       = 0;
      this.foregroundOffset = 0;
      this.platformOffset   = 0;
      this.spriteOffset     = 0;
   },

   makeAllSpritesVisible: function () {
      for (var i=0; i < this.sprites.length; ++i) {
         this.sprites[i].visible = true; 
      }
   },

   reset: function () {
      this.resetOffsets();
      this.resetPlayer();
      this.makeAllSpritesVisible();
      this.canvas.style.opacity = 1.0;
   },

    pollMusic: function () {
        var POLL_INTERVAL = 500, // Poll every 1/2 second
                SOUNDTRACK_LENGTH = 132, // seconds
                timerID;

        timerID = setInterval(function () {
            if (repeatGame.musicElement.currentTime > SOUNDTRACK_LENGTH) {
                clearInterval(timerID);   // Stop polling
                repeatGame.restartMusic(); // Restarts music and polling
            }
        }, POLL_INTERVAL);
    },
    restartMusic: function () {
        repeatGame.musicElement.pause();
        repeatGame.musicElement.currentTime = 0;

        repeatGame.startMusic(); // Calls pollMusic() after 1s delay
    },
    startMusic: function () {
        var MUSIC_DELAY = 1000;


        repeatGame.musicElement.play();


        repeatGame.pollMusic();
    },
    updateScoreElement: function () {
//      this.scoreElement.innerHTML = this.score;
        this.scoreElement = this.score;
    },
       win: function()
    {   //this.scoreElement.innerHTML = 'Winner!';
        this.fgVelocity = this.FOREGROUND_VELOCITY / 20;
        this.playing = false;

    },
    processRightTap: function () {
      if (repeatGame.player.direction === repeatGame.LEFT ||
          repeatGame.fgVelocity === 0) {
         repeatGame.turnRight();
      }
      else {
         repeatGame.player.jump();
      }
   },

   processLeftTap: function () {
      if (repeatGame.player.direction === repeatGame.RIGHT) {
         repeatGame.turnLeft();
      }
      else {
         repeatGame.player.jump();
      }
   },

   touchStart: function (e) {
      if (repeatGame.playing) {
         // Prevent players from inadvertently 
         // dragging the game canvas
         e.preventDefault(); 
      }
   },

   touchEnd: function (e) {
      var x = e.changedTouches[0].pageX;

      if (repeatGame.playing) {
         if (x < repeatGame.canvas.width/2) {
            repeatGame.processLeftTap();
         }
         else if (x > repeatGame.canvas.width/2) {
            repeatGame.processRightTap();
         }

         // Prevent players from double
         // tapping to zoom into the canvas

         e.preventDefault(); 
      }
   },

   addTouchEventHandlers: function () {
      repeatGame.canvas.addEventListener(
         'touchstart', 
         repeatGame.touchStart
      );

      repeatGame.canvas.addEventListener(
         'touchend', 
         repeatGame.touchEnd
      );
   },

    drawMobileDivider: function (cw, ch) {
      this.context.beginPath();
      this.context.moveTo(cw/2, 0);
      this.context.lineTo(cw/2, ch);
      this.context.stroke();
   },

    gameOver: function () {
        //this.instructionsElement.style.opacity = 0.2; 
        //this.soundAndMusicElement.style.opacity = 0.2;

        this.fgVelocity = this.FOREGROUND_VELOCITY / 20;
        this.playing = false;
        alert("GAME OVER YOU LOOSE!");
        
    },
    startGame: function ()
    {
        this.revealInitialToast();
        requestNextAnimationFrame(this.animate);
        this.startMusic();
        
        // this.draw();
        /* this.drawbackgroundstar();
         this.drawForeground();*/
    },
    detectMobile: function () {
      repeatGame.mobile = 'ontouchstart' in window;
   },
 
     resizeElement: function (element, w, h) {
      element.style.width  = w + 'px';
      element.style.height = h + 'px';
   },

   resizeElementsToFitScreen: function (arenaWidth, arenaHeight) {
      repeatGame.resizeElement(
         document.getElementById('repeatgame-arena'), 
         arenaWidth, arenaHeight);

      repeatGame.resizeElement(
                              arenaWidth, arenaHeight);

      repeatGame.resizeElement(
                              arenaWidth, arenaHeight);
   },

   calculateArenaSize: function (viewportSize) {
      var DESKTOP_ARENA_WIDTH  = 800,  // Pixels
          DESKTOP_ARENA_HEIGHT = 520,  // Pixels
          arenaHeight,
          arenaWidth;

      arenaHeight = viewportSize.width * 
                    (DESKTOP_ARENA_HEIGHT / DESKTOP_ARENA_WIDTH);

      if (arenaHeight < viewportSize.height) { // Height fits
         arenaWidth = viewportSize.width;      // Set width
      }
      else {                                   // Height does not fit
         arenaHeight = viewportSize.height;    // Recalculate height
         arenaWidth  = arenaHeight *           // Set width
                      (DESKTOP_ARENA_WIDTH / DESKTOP_ARENA_HEIGHT);
      }

      if (arenaWidth > DESKTOP_ARENA_WIDTH) {  // Too wide
         arenaWidth = DESKTOP_ARENA_WIDTH;     // Limit width
      } 

      if (arenaHeight > DESKTOP_ARENA_HEIGHT) { // Too tall
         arenaHeight = DESKTOP_ARENA_HEIGHT;    // Limit height
      }

      return { 
         width:  arenaWidth, 
         height: arenaHeight 
      };
   },
   getViewportSize: function () {
      return { 
        width: Math.max(document.documentElement.clientWidth ||
               window.innerWidth || 0),  
               
        height: Math.max(document.documentElement.clientHeight ||
                window.innerHeight || 0)
      };
   },

   fitScreen: function () {
      var arenaSize = repeatGame.calculateArenaSize(
                         repeatGame.getViewportSize());

      repeatGame.resizeElementsToFitScreen(arenaSize.width, 
                                          arenaSize.height);
   }


};
// Event handlers.......................................................

window.onkeydown = function (e) {
    var key = e.keyCode;

    if (key === 37) { // 'd' or left arrow
        repeatGame.turnLeft();
    } else if (key === 39) { // 'k' or right arrow
        repeatGame.turnRight();
    } else if (key === 32) { // 'Spacebar'   (e)
        repeatGame.player.jump();
    }

};





// LAunches game

var repeatGame = new RepeatGame();
repeatGame.initializeImages();
repeatGame.createSprites();

repeatGame.detectMobile();

if (repeatGame.mobile) {
   repeatGame.DEFAULT_RUNNING_SLOWLY_THRESHOLD = 30; // fps

   
   repeatGame.addTouchEventHandlers();

   if (/android/i.test(navigator.userAgent)) {
      repeatGame.cannonSound.position = 5.4;
      repeatGame.coinSound.position = 4.8;
      repeatGame.electricityFlowingSound.position = 0.3;
      repeatGame.explosionSound.position = 2.8;
      repeatGame.pianoSound.position = 3.5;
      repeatGame.thudSound.position = 1.8;
   }
}

repeatGame.fitScreen();
window.addEventListener("resize", repeatGame.fitScreen);
window.addEventListener("orientationchange", repeatGame.fitScreen);
   

